package com.indiamart.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.UserDTO;
import com.indiamart.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {

	ResponseModelList<UserDTO> responseModel = new ResponseModelList<UserDTO>();

	@Autowired
	UserService user;

	@GetMapping("/findById/{userId}")
	public ResponseModelList<UserDTO> findByUserId(@PathVariable("userId") String userId) {
		return user.findByUserId(userId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<UserDTO> save(@RequestBody UserDTO userDTO) {
		return user.save(userDTO);
	}

	@GetMapping("/deleteById/{userId}")
	public ResponseModelList<UserDTO> deleteById(@PathVariable("userId") String userId) {
		return user.deleteById(userId);
	}

	@GetMapping("/findAll")
	public ResponseModelList<UserDTO> findAll() {
		return user.findAll();
	}

	@DeleteMapping("/deleteAll")
	public ResponseModelList<UserDTO> deleteAll() {
		return user.deleteAll();
	}

}
