// default package
// Generated May 21, 2020 10:11:13 PM by Hibernate Tools 5.2.10.Final
package com.indiamart.dtos;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.indiamart.baseModelAndDto.BaseDTO;

import lombok.Data;

/**
 * Order generated by hbm2java
 */
@Data
public class OrderDTO extends BaseDTO {

	private String orderId;
	private AddtocartDTO addtocart;
	private UserDTO user;
	private Float orderAmount;
	private String orderShipName;
	private String orderShipAdress2;
	private String orderCity;
	private String orderState;
	private String orderCountry;
	private String orderphone;
	private List<InvoiceDTO> invoices = new ArrayList<InvoiceDTO>(0);
}
