package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Product;

@Repository
public interface ProductDAO extends JpaRepository<Product, String>{

}
