/*
 * package com.indiamart.helpers;
 * 
 * import java.nio.charset.StandardCharsets;
 * 
 * import java.util.Map;
 * 
 * import javax.mail.internet.MimeMessage;
 * 
 * import org.springframework.mail.javamail.JavaMailSender; import
 * org.springframework.mail.javamail.MimeMessageHelper; import
 * org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
 * 
 * import freemarker.template.Template;
 * 
 * public class EmailExtension extends Thread { String to; String subject;
 * Map<String, Object> map;
 * 
 * Template template;
 * 
 * JavaMailSender emailSender;
 * 
 * public EmailExtension(String to, String subject, Map<String, Object> map,
 * Template template, JavaMailSender emailSender) { this.to = to; this.subject =
 * subject; this.map = map;
 * 
 * this.template = template; this.emailSender = emailSender;
 * 
 * }
 * 
 * public void run() { sendMail(); }
 * 
 * public void sendMail() { try {
 * 
 * MimeMessage message = emailSender.createMimeMessage(); MimeMessageHelper
 * helper = new MimeMessageHelper(message,
 * MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
 * StandardCharsets.UTF_8.name()); String html =
 * FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
 * helper.setTo(to); helper.setText(html, true); helper.setSubject(subject);
 * emailSender.send(message); } catch (Exception e) { e.printStackTrace(); }
 * 
 * }
 * 
 * }
 */