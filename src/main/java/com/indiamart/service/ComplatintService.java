package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.ComplatintDTO;

public interface ComplatintService {
	
	ResponseModelList<ComplatintDTO> findByComplatintId(String complatintId);
	ResponseModelList<ComplatintDTO> save(ComplatintDTO complatintDTO);
	ResponseModelList<ComplatintDTO> deleteById(String complatintId);
	ResponseModelList<ComplatintDTO> findAll();
	ResponseModelList<ComplatintDTO> deleteAll();
	

}
