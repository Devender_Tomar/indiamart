package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.ProductDTO;

public interface ProductService {

	ResponseModelList<ProductDTO> findByProductId(String productId);
	ResponseModelList<ProductDTO> save(ProductDTO productDTO);
	ResponseModelList<ProductDTO> deleteById(String productID);
	ResponseModelList<ProductDTO> findAll();
	ResponseModelList<ProductDTO> deleteAll();
}