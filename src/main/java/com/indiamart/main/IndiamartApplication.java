package com.indiamart.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@EntityScan(basePackages = {"com.indiamart.model"})
@SpringBootApplication
public class IndiamartApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndiamartApplication.class, args);
	}

}
